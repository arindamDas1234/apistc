const express = require('express');

const userRouter = express.Router();
const { body, validationResult, check } = require('express-validator');
const passport = require("passport");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const token = require("../auth/key.js");

const db = require("../models");
const user = require('../models/user.js');
// createusers router api

userRouter.post("/create_new_user", [check("username").notEmpty().withMessage("User name must required"), check("mobilenumber").notEmpty().withMessage("Mobile number must required"), check("state").notEmpty().withMessage("User  state is required"),
	check("district").notEmpty().withMessage("district must reuqired"),
	check("pincode").notEmpty().withMessage("user pincode must required"),
	check("address").notEmpty().withMessage("user address must required"),
	check("password").isLength({min:8}).withMessage("Password must be atlist 8 charector")
], async function (req, res) {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		} else {
			// check user mmobile number
			const checknumber =  await db.Users.findOne({ where: { mobileNumber: req.body.mobilenumber} });
			console.log(checknumber);
			if (checknumber) {
				return res.status(420).json({
					error: "Mobile number allready Registered",
					status: false,
					code: 420
				});
			} else {
				let paswordHash = await bcrypt.hashSync(req.body.password, 12);
				try {
				await	db.Users.create({
						user_name: req.body.username,
						mobileNumber: req.body.mobilenumber,
						state: req.body.state,
						distric: req.body.district,
						pinocode: req.body.pincode,
						address: req.body.address,
						password: paswordHash.toString()
					});
				} catch (error) {
					console.log(error);
				}
				res.status(200).json({
					message: "User create successfully",
					success: true
				});
			}
   }
});

// login router api

userRouter.post("/login", [check("mobilenumber").notEmpty().withMessage("Mobile number required"), check("mobilenumber").isLength({ min: 10 }).withMessage("Mobile number is minimum 10 charector"), check("password").isLength({ min: 8 }).withMessage("password length is minimum 8 charector")],async  function (req, res, next) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
	 	res.status(400).json({
			errors: errors.array(),
			status: false
		});
		
	} else {
		// check password
		const Number =  await  db.Users.findOne({ where: { mobileNumber: req.body.mobilenumber } });
		if (Number == true) {
			console.log(true);
		}
		console.log(false);
	
		if (Number !== null) {
				// check password validation
			const pass = await bcrypt.compare(req.body.password, Number.password);
			console.log(pass);
			if (pass) {
				const payload = {
					id: Number.id,
					username: Number.user_name
				};
				const BearerToken = jwt.sign({ payload }, token.key);
				console.log(BearerToken);

				res.status(200).json({
					success: true,
					token: `Bearer ${BearerToken}`
				});
				}
		} else {
			res.status(400).json({
				error: "Mobile number not valid",
				status: false
			});
		}
	}
});

userRouter.get("/userProfile", passport.authenticate('jwt', { session: false }), function (req, res, next) {
	res.json({
		user: req.user
	});
})

module.exports = userRouter;
const { createPool } = require("mysql");

require("dotenv").config();

const pool = createPool({
	port: process.env.DB_PORT,
	host: process.env.DB_HOST,
	password: process.env.DB_PASS,
	user: "root",
	database: process.env.MYSQL_DB,
	connectionLimit: 20
});

module.exports = pool;
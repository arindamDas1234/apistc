const express = require("express");
const cors = require("./utils/cors");

const morgan = require("morgan");

const path = require("path");
const cluster = require("cluster");
const os = require("os");
const http = require("http");
const bodyParser = require("body-parser");
const error = require("http-errors");
require("dotenv").config();
const app = express();

const router = require("./router/userRouter");
const db = require("./models");
const passport = require("passport");

app.use(morgan("dev"));

app.use(express.static(path.join(__dirname, "public")));

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cors);
app.use(passport.initialize());

require("./auth/passport.js")(passport);

// user router
app.use('/api/user', router);

if (cluster.isMaster) {
	for (let i = 0; i < os.cpus().length; i++){
		cluster.fork();
	}
	// get more number of cpus from local Machine
	let wokrerThred = [];
	for (const id in cluster.workers) {

		wokrerThred.push(id);
	}

// notify id worker is dead
// notify if any worker is died
wokrerThred.forEach(async (pid) => {
		console.log(pid);
	cluster.workers[pid].send({
		from: "isMaster",
		type: "SIGKILL",
		message: "clean up ! workr is died and start new worker "
	});
});
// check worker status
if (process.env.NODE_ENV == "production") {
	cluster.on("online", function (worker) {
		if (worker.isConnected()) {
			console.log(`worker is now online ${worker.process.pid}`);
		}
	});
	cluster.on("exit", function (worker, code, signal) {
		if (worker.isDead()) {
			console.log(`worker is died ${worker.process.pid}`);
		}
		cluster.fork();
	});

	cluster.on("message", function (worker, { from, message }) {
		console.log(`Worker ${worker.process.pid} from ${from} ${message} `);
	});
}
} else {
	if (cluster.isWorker) {
		process.on("message", function (msg) {
			if (msg.from == "isMaster" && msg.type =="SIGKILL") {
				process.send({
					from: "isWorker",
					message: `yes master ${msg.from}`
				});
			}
		});

		process.on("exit", function () {
			process.exit(process.exitCode);
		});
	var port = process.env.PORT;
		const server = http.createServer(app);
		db.sequelize.sync().then((req) => {

			server.listen(port, function () {
				console.log(`erver runnning on port ${port}`);
			})
		})
	}

}
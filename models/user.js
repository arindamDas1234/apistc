module.exports = (sequelize, DataTypes) => {
	var Users = sequelize.define("Users", {
		id: {
			type:  DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		user_name: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: true
			}
		},

		mobileNumber: {
			type: DataTypes.INTEGER,
			allowNull: false,
			validate: {
				notEmpty: false
			}
		},
		state: {
			type: DataTypes.INTEGER,
			allowNull: true,
			required: false,
			validate: {
				notEmpty:true
			}
		},
		distric: {
			type: DataTypes.STRING,
			allowNull: true,
			required: false,
			validate: {
				notEmpty:true
			}
		},
		pinocode: {
			type: DataTypes.INTEGER,
			allowNull: true,
			validate: {
				notEmpty:true
			}
		},
		address: {
			type: DataTypes.TEXT,
			allowNull: false,
			validate: {
				notEmpty:true
			}
		},
		image: {
			type: DataTypes.STRING,
			allowNull: true,
			validate: {
				notEmpty:false
			}
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty:false
			}
		},
		created_at: {
			field: 'created_at',
			type:DataTypes.DATE
		},
		updated_at: {
			field: 'updated_at',
			type:DataTypes.DATE
		}
	});

	return Users;
}
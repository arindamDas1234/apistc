module.exports = (sequelize, DataTypes) => {
	var Roles = sequelize.define('Roles', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		role_name: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: true
			}
		},
		role_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			validate: {
				notEmpty:true
			}
		}
	});


	return Roles;
}
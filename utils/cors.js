const cors = (req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Origin-X-Request-Headers", "Origin, Content-Type, Access, X-Request-With");

	next();
}

module.exports = cors;
const JwtStrategy = require("passport-jwt").Strategy;

const ExtractJwt = require("passport-jwt").ExtractJwt;
const tokenKey = require("./key.js");
const db = require("../models");

var opts = {};

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

opts.secretOrKey = tokenKey.key;

module.exports = passport => {
	passport.use(
		new JwtStrategy(opts, async function (jwt_payload, done) {

		const user =await db.Users.findOne({ where: { id: jwt_payload.payload.id } })
			if (user) {
				return done(null, user);
			} else {
				return done(null, false);
			}
		})
	)
}